CC = g++ --std=c++11 -O1 -g

unittest : unittest.cpp
	$(CC) -o $@ $^

benchmark : benchmark.cpp core/hpack.cpp
	$(CC) -o $@ $^
	
all : unittest benchmark
