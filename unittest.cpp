#define ERICHPACKDEBUGFLAG 1
#include <memory>

#include "core/hpack.h"
#include "core/hpack.cpp"
using namespace EricHpack;

void TestIntegerEnDe(Integer2byte prefix,Uinteger5byte i) {
    HpackStatus stat;
    std::vector<unsigned char> buff;
    VecHpWBuffer wb(buff);
    VecHpRBuffer rb(buff);

    printf("TestIntegerEnDe(prefix:%d, i:%lu)\n", prefix, i);
    if((stat = IntegerEncode(wb, prefix, i)) != HpackStatus::Success) {
        printf("TestIntegerEnDe fail at IntegerEncode %s.\n",HpackStatus2CString(stat));
    }
    BufferPrint(buff);
    i = 0;
    if(IntegerDecode(rb, prefix, i) == HpackStatus::Success) {
        printf("TestIntegerEnDe:%lu\n", i);
    }
}

void _TestHpXBuffer(HpRBuffer &rb) {
    do {
        printf("%s ",LsbByte(rb.Current()));
    } while(rb.Next());
    for(int i = 0; i<10; ++i) {
        if(rb.Next()) {
            printf("HpRBuffer not keep End.\n\n");
        }
    }
    printf("\n");
}

void TestHpXBuffer(const std::vector<unsigned char> &buff) {
    size_t i =0;
    unsigned char buffmem[1024];
    std::shared_ptr<HpRBuffer> rb = std::make_shared<VecHpRBuffer>(buff);
    printf("\nVecHpRBuffer\n");
    _TestHpXBuffer(*(rb.get()));
    i = 0;
    for( unsigned char c : buff ) {
        buffmem[i++] = c;
    }
    std::shared_ptr<ArrHpRBuffer> ra = std::make_shared<ArrHpRBuffer>(buffmem, buff.size());
    printf("ArrHpRBuffer\n");
    _TestHpXBuffer(*(ra.get()));
}

std::shared_ptr<Hpack> TestHpackDecoder(std::vector<unsigned char> &buff,std::shared_ptr<Hpack> hpack = {}) {
    if(!hpack) {
        hpack = std::make_shared<Hpack>();
    }
    std::vector<KeyValPair> header;

    printf("%s\nHeader:\n", Hpack::HpackStatus2String(hpack->Decoder(Hpack::MakeHpRBuffer(buff), header)).c_str());
    for(KeyValPair &kvp : header) {
        KeyValPairPrint(kvp);
    }
    IndexTablePrint(hpack->_IndexTable());
    buff.clear();
    return hpack;
}

std::shared_ptr<Hpack> TestHpackEncoder(std::vector<KeyValPair> &header,std::vector<unsigned char> &ans,std::shared_ptr<Hpack> hpack = {}) {
    std::vector<unsigned char> buff;
    std::shared_ptr<HpWBuffer> stream;
    if(!hpack) {
        hpack = std::make_shared<Hpack>();
    }
    stream = Hpack::MakeHpWBuffer(buff);
    printf("hpack::Encoder %s\n",Hpack::HpackStatus2String(hpack->Encoder(*(stream.get()), header, StringLiteralEncodeWay::EShortest)).c_str());
    if(buff != ans) {
        printf("TestHpackEncoder not match anser.\n");
        for(const KeyValPair &kvp : header) {
            KeyValPairPrint(kvp);
        }
        BufferPrint(buff);
        BufferPrint(ans);
    }
    IndexTablePrint(hpack->_IndexTable());
    ans.clear();
    header.clear();
    return hpack;
}

std::shared_ptr<Hpack> TestHpackDynamicTableUpdate(std::vector<unsigned char> &buffans, size_t ans, std::shared_ptr<Hpack> hpack = {}) {
    HpackStatus stat;
    HeaderFieldInfo hfdi;
    std::vector<KeyValPair> header;
    if(!hpack) {
        hpack = std::make_shared<Hpack>();
    }
    if(buffans.size() == 0) {
        Hpack thpack;
        std::shared_ptr<HpWBuffer> stream = std::make_shared<VecHpWBuffer>(buffans);
        if((stat = thpack.EncoderDynamicTableSizeUpdate(*(stream.get()), ans, hfdi)) != HpackStatus::Success) {
            printf("Hpack::EncoderDynamicTableSizeUpdate fail %s.\n", Hpack::HpackStatus2String(stat).c_str());
        }
    }
    if((stat = hpack->Decoder(Hpack::MakeHpRBuffer(buffans), header)) != HpackStatus::Success) {
        printf("Hpack::Decoder fail %s.\n", Hpack::HpackStatus2String(stat).c_str());
    }
    if(hpack->DynamicTableSize() > ans) {
        printf("Hpack Decoder Dynamic Table Size Update fail DynamicTableSize(%ju) should less then %ju.\n", (uintmax_t)hpack->DynamicTableSize(), (uintmax_t)ans);
    }
    buffans.clear();
    return hpack;
}

std::shared_ptr<Hpack> TestHpackDynamicTableOverflow(size_t newsize,size_t dynamicTableMaxSize,std::shared_ptr<Hpack> hpack = {}) {
    HpackStatus stat;
    HeaderFieldInfo hfdi;
    std::vector<unsigned char> buff;
    static size_t DynamicTableMaxSize = dynamicTableMaxSize;
    VecHpWBuffer hprbuff(buff);
    if(!hpack) {
        hpack  = std::make_shared<Hpack>(&DynamicTableMaxSize);
    }
    stat = hpack->EncoderDynamicTableSizeUpdate(hprbuff, newsize, hfdi);
    if(newsize > dynamicTableMaxSize) {
        if(stat != HpackStatus::TableSizeOverflow) {
            printf("TestHpackDynamicTableOverflow fail Expect to return TableSizeOverflow but return %s.", Hpack::HpackStatus2String(stat).c_str());
        }
    } else {
        if(stat != HpackStatus::Success) {
            printf("Hpack::EncoderDynamicTableSizeUpdate fail %s.\n", Hpack::HpackStatus2String(stat).c_str());
        }
    }
    return hpack;
}

int main() {
    // will dose not free Memory
    if(std::setvbuf(stdout, nullptr, _IONBF, 0) != 0) {
        printf("Fail at set stdout buffer to _IONBF.\n");
    }
    printf("Test LsbByte...\n");
    printf("%s\n",LsbByte(1));
    printf("%s\n",LsbByte(3));
    printf("%s\n",LsbByte(254));

    printf("Test IndexTable...\n");
    size_t evictcn;
    KeyValPair kvp;
    bool onlymatchkey;
    size_t headerTableSize = SETTINGS_HEADER_TABLE_SIZE_DEFAULT;
    IndexTable inxt(&headerTableSize);
    inxt.at(1, kvp);
    KeyValPairPrint(kvp);
    inxt.at(2, kvp);
    if(inxt.at(StaticTableSize - 1,kvp)  == HpackStatus::TableIndeNotExist) {
        printf("IndexTable fail at not return TableIndeNotExist.\n");
    }
    kvp.first = kvp.second = std::string("0123456789");
    inxt.DynamicTableInsert(kvp, evictcn);
    inxt.at(StaticTableSize,kvp);
    KeyValPairPrint(kvp);
    IndexTablePrint(inxt);
    if(inxt.find(kvp, onlymatchkey) != StaticTableSize) {
        printf("IndexTable fail at find in DynamicTable.\n");
    }
    headerTableSize = 104;
    IndexTable inxtt(&headerTableSize);
    kvp.first = kvp.second = std::string("0123456789");
    inxtt.DynamicTableInsert(kvp, evictcn);
    inxtt.DynamicTableInsert(kvp, evictcn);
    IndexTablePrint(inxtt);
    kvp.first = kvp.second = std::string("01234567899");
    inxtt.DynamicTableInsert(kvp, evictcn);
    if (evictcn != 2) {
        printf("Not expect evictcn %lu\n", evictcn);
    }
    printf("Test HpXBuffer...\n");
    std::vector<unsigned char> buff;
    std::shared_ptr<HpWBuffer> wb = std::make_shared<VecHpWBuffer>(buff);
    std::shared_ptr<HpRBuffer> rb = std::make_shared<VecHpRBuffer>(buff);

    wb->OrCurrent(0x03);
    wb->Append(0x04);
    wb->Append(0x05);
    BufferPrint(buff);
    TestHpXBuffer(buff);
    buff.clear();
    wb->OrCurrent(0x00);
    for(unsigned char i = 1; i<255; i++) {
        wb->Append(i);
    }
    TestHpXBuffer(buff);
    buff.clear();

    printf("Test Integer Decode Encode...\n");
    TestIntegerEnDe(5,10);
    TestIntegerEnDe(5,1337);
    TestIntegerEnDe(5,31);
    TestIntegerEnDe(6,63);
    TestIntegerEnDe(4,15);
    TestIntegerEnDe(7,8);
    TestIntegerEnDe(5,654321);
    TestIntegerEnDe(6,654321);
    TestIntegerEnDe(7,654321);
    TestIntegerEnDe(5,1234);

    printf("Test StringLiteral Decode...\n");
    std::string str;
    BufferFromHex("0a 6375 7374 6f6d 2d6b 6579", buff);
    rb = std::make_shared<VecHpRBuffer>(buff);
    if(StringLiteralDecode(*rb, str) == HpackStatus::Success) {
        printf("%s\n", str.c_str());
    } else {
        printf("StringLiteral Decode fail.\n");
        BufferPrint(buff);
    }
    str.clear();
    buff.clear();

    BufferFromHex("8cf1e3 c2e5 f23a 6ba0 ab90 f4ff", buff);
    rb = std::make_shared<VecHpRBuffer>(buff);
    if(StringLiteralDecode(*rb, str) == HpackStatus::Success) {
        printf("%s\n", str.c_str());
    } else {
        printf("StringLiteral Decode fail.\n");
        BufferPrint(buff);
    }
    str.clear();
    buff.clear();

    printf("Test StringLiteral Encode...\n");
    wb = std::make_shared<VecHpWBuffer>(buff);
    rb = std::make_shared<VecHpRBuffer>(buff);
    StringLiteralEncode<StringLiteralEncodeWay::EHuffman>(*(wb.get()), "www.example.com");
    StringLiteralDecode(*(rb.get()), str);
    printf("%s\n", str.c_str());
    buff.clear();
    str.clear();

    wb = std::make_shared<VecHpWBuffer>(buff);
    rb = std::make_shared<VecHpRBuffer>(buff);
    StringLiteralEncode<StringLiteralEncodeWay::EOctet>(*(wb.get()), "/w/cpp/language/template_parameters");
    StringLiteralDecode(*(rb.get()), str);
    printf("%s\n", str.c_str());
    buff.clear();
    str.clear();

    wb = std::make_shared<VecHpWBuffer>(buff);
    rb = std::make_shared<VecHpRBuffer>(buff);
    StringLiteralEncode<StringLiteralEncodeWay::EHuffman>(*(wb.get()), "ab_testing_id=%22ee51c1ab-004a-48ac-88fa-62aafc2a39c3%22; Max-Age=31536000; Domain=substack.com; Path=/; Expires=Fri, 20 Dec 2024 07:32:01 GMT; HttpOnly; Secure; SameSite=Lax, __cf_bm=SRtuA8QBZVz8ikCsBwpOHRClxl4QQMwWaTYXeWMugyk-1703143921-1-Afd6A/Ia7gKwvluIpmSt+mgEYaAC7lbtqpwK2QyJSN0uFIa/ZVUKI9XcP0c9rjr9VJaWEU762GhbKRDG5+gpPgQ=; path=/; expires=Thu, 21-Dec-23 08:02:01 GMT; domain=.substack.com; HttpOnly; Secure; SameSite=None");
    StringLiteralDecode(*(rb.get()), str);
    printf("%s\n", str.c_str());
    buff.clear();
    str.clear();

    wb = std::make_shared<VecHpWBuffer>(buff);
    rb = std::make_shared<VecHpRBuffer>(buff);
    StringLiteralEncode<StringLiteralEncodeWay::EHuffman>(*(wb.get()), "\x0a\x16");
    StringLiteralDecode(*(rb.get()), str);
    printf("%s\n", _String2Hex(str).c_str());
    buff.clear();
    str.clear();

    printf("Test Hpack Decoder Dynamic Table Size Update...\n");
    BufferFromHex("23", buff);
    TestHpackDynamicTableUpdate(buff, 3);
    BufferFromHex("3f b309", buff);
    TestHpackDynamicTableUpdate(buff, 1234);
    TestHpackDynamicTableUpdate(buff, 54);
    TestHpackDynamicTableUpdate(buff, 3421);
    TestHpackDynamicTableUpdate(buff, 2222);
    TestHpackDynamicTableOverflow(100, 1024);
    TestHpackDynamicTableOverflow(1234, 4000);
    TestHpackDynamicTableOverflow(2345, 2345);
    TestHpackDynamicTableOverflow(4097, 4098);

    printf("Test Hpack Decoder...\n");
    BufferFromHex("400a 6375 7374 6f6d 2d6b 6579 0d63 7573", buff);
    BufferFromHex("746f 6d2d 6865 6164 6572", buff);
    TestHpackDecoder(buff);

    BufferFromHex("040c 2f73 616d 706c 652f 7061 7468", buff);
    TestHpackDecoder(buff);

    BufferFromHex("1008 7061 7373 776f 7264 0673 6563 726574", buff);
    TestHpackDecoder(buff);

    BufferFromHex("82", buff);
    TestHpackDecoder(buff);

    std::shared_ptr<Hpack> hpack;
    BufferFromHex("8286 8441 8cf1 e3c2 e5f2 3a6b a0ab 90f4ff", buff);
    hpack = TestHpackDecoder(buff);
    BufferFromHex("8286 84be 5886 a8eb 1064 9cbf", buff);
    TestHpackDecoder(buff, hpack);
    BufferFromHex("8287 85bf 4088 25a8 49e9 5ba9 7d7f 8925 a849 e95b b8e8 b4bf", buff);
    TestHpackDecoder(buff, hpack);
    buff.clear();

    printf("Test Hpack Encoder...\n");
    std::vector<KeyValPair> header;
    std::vector<unsigned char> buffans;
    BufferFromHex("8286 8441 8cf1 e3c2 e5f2 3a6b a0ab 90f4ff", buffans);
    header.push_back(std::make_pair(std::string(":method"),std::string("GET")));
    header.push_back(std::make_pair(std::string(":scheme"),std::string("http")));
    header.push_back(std::make_pair(std::string(":path"),std::string("/")));
    header.push_back(std::make_pair(std::string(":authority"),std::string("www.example.com")));
    hpack = TestHpackEncoder(header, buffans);

    BufferFromHex("8286 84be 5886 a8eb 1064 9cbf", buffans);
    header.push_back(std::make_pair(std::string(":method"),std::string("GET")));
    header.push_back(std::make_pair(std::string(":scheme"),std::string("http")));
    header.push_back(std::make_pair(std::string(":path"),std::string("/")));
    header.push_back(std::make_pair(std::string(":authority"),std::string("www.example.com")));
    header.push_back(std::make_pair(std::string("cache-control"),std::string("no-cache")));
    hpack = TestHpackEncoder(header, buffans, hpack);

    BufferFromHex("8287 85bf 4088 25a8 49e9 5ba9 7d7f 8925 a849 e95b b8e8 b4bf", buffans);
    header.push_back(std::make_pair(std::string(":method"),std::string("GET")));
    header.push_back(std::make_pair(std::string(":scheme"),std::string("https")));
    header.push_back(std::make_pair(std::string(":path"),std::string("/index.html")));
    header.push_back(std::make_pair(std::string(":authority"),std::string("www.example.com")));
    header.push_back(std::make_pair(std::string("custom-key"),std::string("custom-value")));
    hpack = TestHpackEncoder(header, buffans, hpack);

    printf("Test Hpack Encoder With Evict...\n");
    size_t settingheadertablesize = 256;
    hpack = std::make_shared<Hpack>(&settingheadertablesize);
    BufferFromHex("4882 6402 5885 aec3 771a 4b61 96d0 7abe", buffans);
    BufferFromHex("9410 54d4 44a8 2005 9504 0b81 66e0 82a6", buffans);
    BufferFromHex("2d1b ff6e 919d 29ad 1718 63c7 8f0b 97c8", buffans);
    BufferFromHex("e9ae 82ae 43d3", buffans);
    header.push_back(std::make_pair(std::string(":status"),std::string("302")));
    header.push_back(std::make_pair(std::string("cache-control"),std::string("private")));
    header.push_back(std::make_pair(std::string("date"),std::string("Mon, 21 Oct 2013 20:13:21 GMT")));
    header.push_back(std::make_pair(std::string("location"),std::string("https://www.example.com")));
    hpack = TestHpackEncoder(header, buffans, hpack);

    BufferFromHex("4883 640e ffc1 c0bf", buffans);
    header.push_back(std::make_pair(std::string(":status"),std::string("307")));
    header.push_back(std::make_pair(std::string("cache-control"),std::string("private")));
    header.push_back(std::make_pair(std::string("date"),std::string("Mon, 21 Oct 2013 20:13:21 GMT")));
    header.push_back(std::make_pair(std::string("location"),std::string("https://www.example.com")));
    hpack = TestHpackEncoder(header, buffans, hpack);

    BufferFromHex("88c1 6196 d07a be94 1054 d444 a820 0595", buffans);
    BufferFromHex("040b 8166 e084 a62d 1bff c05a 839b d9ab", buffans);
    BufferFromHex("77ad 94e7 821d d7f2 e6c7 b335 dfdf cd5b", buffans);
    BufferFromHex("3960 d5af 2708 7f36 72c1 ab27 0fb5 291f", buffans);
    BufferFromHex("9587 3160 65c0 03ed 4ee5 b106 3d50 07", buffans);
    header.push_back(std::make_pair(std::string(":status"),std::string("200")));
    header.push_back(std::make_pair(std::string("cache-control"),std::string("private")));
    header.push_back(std::make_pair(std::string("date"),std::string("Mon, 21 Oct 2013 20:13:22 GMT")));
    header.push_back(std::make_pair(std::string("location"),std::string("https://www.example.com")));
    header.push_back(std::make_pair(std::string("content-encoding"),std::string("gzip")));
    header.push_back(std::make_pair(std::string("set-cookie"),std::string("foo=ASDJKHQKBZXOQWEOPIUAXQWEOIU; max-age=3600; version=1")));
    hpack = TestHpackEncoder(header, buffans, hpack);

    printf("Test Hpack Encoder Decoder...\n");
    hpack = std::make_shared<Hpack>();
    header.push_back(std::make_pair(std::string(":scheme"),std::string("w")));
    hpack->Encoder(Hpack::MakeHpWBuffer(buff), header);
    TestHpackDecoder(buff);
    buff.clear();
    header.clear();
    return 0;
}
