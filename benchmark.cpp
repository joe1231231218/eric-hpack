#ifndef ERICHPACKDEBUGFLAG
#define ERICHPACKDEBUGFLAG 1
#endif // ERICHPACKDEBUGFLAG
#if ERICHPACKDEBUGFLAG == 1
#define RESULTCHECK 1
#endif // ERICHPACKDEBUGFLAG

#include "benchutil.cpp"

#include <array>
#include <string>
#include <vector>
#include <chrono>
#include <memory>
#include <algorithm>
#include <functional>

#include <cmath>
#include "core/hpack.h"

using EricHpack::KeyValPair;

const bool BenchmarkFormatCsv = false;
const bool MedianAbsoluteDeviation = false;
const size_t DefaultHeaderCapacity = 1 << 13;
const size_t DefaultBufferCapacity = 1 << 13;
const unsigned long long DeEnBaseLoopTime = 10;
const unsigned long long BenchmarkLoopTime = 40;

void RandomItem() {
    EricHpack::Hpack hpack;
    HeaderVec header;
    std::unique_ptr<Boob> boob;
    std::vector<unsigned char> buff;
    for(const char * filename : LoadBoobLst) {
        boob.reset(new Boob(filename));
        boob->HpRBufferRandom(buff);
        hpack.Decoder(EricHpack::Hpack::MakeHpRBuffer(buff), header);
        buff.clear();
        header.clear();
        printf("%s RandomItem.\n", filename);
    }
    return;
}

void DeEnSubHfdi(const SubFlag &subflag,std::vector<EricHpack::HeaderFieldInfo>& ehfdi, const HeaderVec &feed) {
    EricHpack::HeaderFieldInfo hfdi;
    static const std::array<EricHpack::HeaderFieldRepresentation, 2> plan0 = {EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed,EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldAlwaysIndex};
    static const std::array<EricHpack::HeaderFieldRepresentation, 3> plan1 = {EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed,EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldWithoutIndexing, EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed};
    static const std::array<EricHpack::HeaderFieldRepresentation, 4> plan2 = {EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldWithoutIndexing, EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldWithIncrementalIndexing, EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed, EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldAlwaysIndex};
    for(size_t i = 0 ; i < feed.size(); ++i) {
        switch(subflag.hFDIGen) {
        case SubFlag::HFDIAlwaysIndex:
            hfdi.representation = EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldAlwaysIndex;
            break;
        case SubFlag::HFDIIncrementalIndexing:
            hfdi.representation = EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldWithIncrementalIndexing;
            break;
        case SubFlag::HFDINeverIndexed:
            hfdi.representation = EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed;
            break;
        case SubFlag::HFDIWithoutIndexing:
            hfdi.representation = EricHpack::HeaderFieldRepresentation::RLiteralHeaderFieldWithoutIndexing;
            break;
        case SubFlag::HFDIPlan0:
            hfdi.representation = plan0.at((i % plan0.size()));
            break;
        case SubFlag::HFDIPlan1:
            hfdi.representation = plan1.at((i % plan1.size()));
            break;
        case SubFlag::HFDIPlan2:
            hfdi.representation = plan2.at((i % plan2.size()));
            break;
        default:
            printf("Unexpect SubFlag::hFDIGen(%u) on Encoder.\n",subflag.hFDIGen);
        }
        ehfdi.push_back(hfdi);
    }
    return;
}

void DeEnSub(std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int)>  loadlst,const SubFlag &subflag) {
    int loadstage;
    HeaderVec header;
    HeaderVecPair hpair;
    EricHpack::Hpack reqs, resps,reqr, respr;
    EricHpack::HpackStatus enstat, destat;

    std::vector<std::string> exclude;
    std::vector<unsigned char> buff;
    std::vector<EricHpack::HeaderFieldInfo> ehfdi, dhfdi;

    buff.reserve(DefaultBufferCapacity);
    header.reserve(DefaultHeaderCapacity);
    exclude.assign(subflag.exclude.begin(), subflag.exclude.end());

    auto _internal_sub = [&] (EricHpack::Hpack &send, EricHpack::Hpack &recv, HeaderVec &feed) mutable -> void {
        std::unique_ptr<EricHpack::HpRBuffer> decodeInput;
        std::shared_ptr<InputMemory> ipm;
        auto encodeInput = EricHpack::Hpack::MakeHpWBuffer(buff, 16384);

        if(subflag.encodeTy == SubFlag::EncodeTyPtr) {
            enstat = send.Encoder(std::move(encodeInput),feed);
        } else if(subflag.encodeTy == SubFlag::EncodeTyRef) {
            enstat = send.Encoder(*encodeInput,feed);
        } else if(subflag.encodeTy == SubFlag::EncodeTyWay) {
            enstat = send.Encoder(*encodeInput,feed);
        } else if(subflag.encodeTy == SubFlag::EncodeTyExcludePtr) {
            enstat = send.Encoder(std::move(encodeInput),feed, exclude);
        } else if(subflag.encodeTy == SubFlag::EncodeTyExcludeRef) {
            enstat = send.Encoder(*encodeInput,feed, exclude);
        } else if(subflag.encodeTy == SubFlag::EncodeTyExcludeWay) {
            enstat = send.Encoder(*encodeInput,feed, exclude);
        } else if(subflag.IsEncodeHFDI()) {
            DeEnSubHfdi(subflag, ehfdi, feed);
            enstat = send.Encoder(std::move(encodeInput),feed, ehfdi);
        }

        if(subflag.docodeInput == SubFlag::InputVec) {
            decodeInput = EricHpack::Hpack::MakeHpRBuffer(buff);
        } else if(subflag.docodeInput == SubFlag::InputMem) {
            ipm = std::make_shared<InputMemory>(buff);
            decodeInput = EricHpack::Hpack::MakeHpRBuffer(ipm->start, ipm->sizeByte);
        }

        switch(subflag.decodeTy) {
        case SubFlag::DecodeTyPtr:
            destat = recv.Decoder(std::move(decodeInput), header);
            break;
        case SubFlag::DecodeTyRef:
            destat = recv.Decoder(*decodeInput, header);
            break;
        case SubFlag::DecodeTyHFDIPtr:
            destat = recv.Decoder(std::move(decodeInput), header, dhfdi);
            break;
        case SubFlag::DecodeTyHFDIRef:
            destat = recv.Decoder(*decodeInput, header, dhfdi);
            break;
        }
    };

    for(int i = 0; i < 1; ++i) {
        loadstage = 0;
        while(loadlst(hpair.req, hpair.resp, loadstage++)) {
            _internal_sub(reqs, reqr, hpair.req);
            SubCheckClear(hpair.req, header, "Request", enstat,destat,reqs,reqr, subflag, ehfdi, dhfdi);
            buff.clear();

            _internal_sub(resps, respr, hpair.resp);
            SubCheckClear(hpair.resp, header, "Response", enstat,destat,resps,respr, subflag, ehfdi, dhfdi);
            buff.clear();
        }
    }
    return;
}

void TestItem() {
    int stage;
    bool endStage, statusNext;
    size_t arrpos;
    SubFlag subflag;
    std::string funcorfilename;
    std::unique_ptr<WrapLoadFucntion> wrapfunc;
    const std::array<EricHpack::StringLiteralEncodeWay, 3> literalEncodeWay = {EricHpack::StringLiteralEncodeWay::EOctet, EricHpack::StringLiteralEncodeWay::EHuffman, EricHpack::StringLiteralEncodeWay::EShortest};

    stage = 0;
    arrpos = 0;
    endStage = false;
    subflag.StatusListGen();
    while(stage < 50) {
        do {
            if(!LoadTestItem(funcorfilename,stage, wrapfunc)) {
                endStage = true;
                break;
            }
            if(subflag.IsEncodeExclude()) {
                subflag.exclude.assign({"authorization", "set-cookie", "cookie", "token"});
            }
            //printf("%s Test ", funcorfilename.c_str());
            //subflag.Print();

            DeEnSub(WrapLoadFucntion::Bind(wrapfunc.get()), subflag);

            if(subflag.IsEncodeWay()) {
                if(arrpos < literalEncodeWay.size()) {
                    statusNext = true;
                    subflag.encodeWay = literalEncodeWay.at(arrpos++);
                } else {
                    arrpos = 0;
                    statusNext = subflag.StatusNext();
                }
            } else {
                statusNext = subflag.StatusNext();
            }
        } while(statusNext);
        if(endStage) {
            break;
        }
        stage++;
        printf("%s run TestItem.\n", funcorfilename.c_str());
    }
    return;
}

void _DeEnDynamicTableUpdate(const HeaderVec &feed, EricHpack::Hpack &send, EricHpack::Hpack &recv, const std::string &failat, const size_t hlstpos) {
    HeaderVec header, h, f;
    const SubFlag subflag;
    EricHpack::HpackStatus enstat, destat;
    EricHpack::HeaderFieldInfo hfdi;
    std::vector<unsigned char> buffpre, buff;
    std::vector<EricHpack::HeaderFieldInfo> hfdilst;

    if(hlstpos % 7 == 0) {
        std::unique_ptr<EricHpack::HpWBuffer> wstream = std::move(EricHpack::Hpack::MakeHpWBuffer(buffpre));
        send.EncoderDynamicTableSizeUpdate(*(wstream.get()),(hlstpos % 11) * 150, hfdi);
    }
    enstat = send.Encoder(EricHpack::Hpack::MakeHpWBuffer(buff, 12288), feed);
    buffpre.insert(buffpre.end(), buff.begin(), buff.end());
    destat = recv.Decoder(EricHpack::Hpack::MakeHpRBuffer(buffpre), header);
    for(const std::pair<std::string,std::string> &kvp : feed) {
        if(kvp.first.size() != 0 || kvp.second.size() != 0) {
            f.push_back(kvp);
        }
    }
    for(const std::pair<std::string,std::string> &kvp : header) {
        if(kvp.first.size() != 0 || kvp.second.size() != 0) {
            h.push_back(kvp);
        }
    }
    SubCheckClear(f, h, failat, enstat, destat, send, recv, subflag, hfdilst, hfdilst);
}

void DeEnDynamicTableUpdate(const std::vector<std::shared_ptr<HeaderVecPair> > &hlst) {
    size_t settingheadertablesize = 16384;
    EricHpack::Hpack reqs(&settingheadertablesize), resps(&settingheadertablesize),reqr(&settingheadertablesize), respr(&settingheadertablesize);
    for(size_t i = 0; i < hlst.size() ; ++i) {
        _DeEnDynamicTableUpdate(hlst.at(i)->req, reqs, reqr, "Request", i);
        _DeEnDynamicTableUpdate(hlst.at(i)->resp, resps, respr, "Response", i);
    }
    return;
}

void DynamicTableUpdateItem() {
    int stage;
    std::string funcorfilename;
    std::vector<std::shared_ptr<HeaderVecPair> > hlst;

    stage = 0;
    while(LoadDynamicTableUpdateItem(funcorfilename, hlst, stage) && stage < 50) {
        stage++;
        DeEnDynamicTableUpdate(hlst);
        printf("%s run DynamicTableUpdateItem.\n", funcorfilename.c_str());
    }

    return;
}

unsigned long DeEnBase(const std::vector<std::shared_ptr<HeaderVecPair> > &hlst) {
    unsigned long durmicro;
    HeaderVec qheader, pheader;
    EricHpack::Hpack reqs, resps,reqr, respr;
    EricHpack::HpackStatus enstat, destat;
    std::vector<unsigned char> qbuff, pbuff;
    std::chrono::steady_clock::time_point start;
    qheader.reserve(DefaultHeaderCapacity);
    pheader.reserve(DefaultHeaderCapacity);
    qbuff.reserve(DefaultBufferCapacity);
    pbuff.reserve(DefaultBufferCapacity);
    start = std::chrono::steady_clock::now();
    for(unsigned long long i = 0; i < DeEnBaseLoopTime; ++i) {
        for(const std::shared_ptr<HeaderVecPair>  &hpair : hlst) {
            enstat = reqs.Encoder(EricHpack::Hpack::MakeHpWBuffer(qbuff, 12288),hpair->req);
            destat = reqr.Decoder(EricHpack::Hpack::MakeHpRBuffer(qbuff), qheader);
#if RESULTCHECK
            DeEnFailCheck(hpair->req, qheader, "Request", enstat,destat);
#endif // RESULTCHECK
            enstat = resps.Encoder(EricHpack::Hpack::MakeHpWBuffer(pbuff, 12288), hpair->resp);
            destat = respr.Decoder(EricHpack::Hpack::MakeHpRBuffer(pbuff), pheader);
#if RESULTCHECK
            DeEnFailCheck(hpair->resp, pheader, "Response",enstat,destat);
#endif // RESULTCHECK
            qbuff.clear();
            pbuff.clear();
            qheader.clear();
            pheader.clear();
        }
    }
    durmicro = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - start).count();
    return durmicro;
}

void BenchmarkItem() {
    int stage;
    double median, mad, average;
    unsigned long long madt;
    const unsigned long long truncateN = BenchmarkLoopTime / 10;
    std::string funcorfilename;
    std::array<double, BenchmarkLoopTime> microsecond;
    std::vector<std::shared_ptr<HeaderVecPair> > hlst;
    std::chrono::steady_clock::time_point start;

    stage = 0;
    if(BenchmarkFormatCsv) {
        while(LoadBenchmarkItem(funcorfilename, hlst,stage) && stage < 50) {
            printf("%s,", funcorfilename.c_str());
            ++stage;
        }
        printf("\n");
    }

    stage = 0;
    while(LoadBenchmarkItem(funcorfilename, hlst,stage) && stage < 50) {
        stage++;
        for(unsigned long long i = 0; i < BenchmarkLoopTime; ++i) {
            microsecond[i] = DeEnBase(hlst);
        }
        std::sort(microsecond.begin(), microsecond.end());
        if(MedianAbsoluteDeviation) {
            median = microsecond.at(BenchmarkLoopTime / 2);
            if(BenchmarkFormatCsv) {
                printf("%f,",median);
            } else {
                madt = 0;
                for(unsigned long long i = truncateN; i < BenchmarkLoopTime - truncateN; ++i) {
                    madt = madt + (unsigned long long)fabs(microsecond.at(i) - median);
                }
                mad = (double)madt / (double)(BenchmarkLoopTime - truncateN * 2);
                printf("%s run %llu time in Median %.0lf microsecond With Median absolute deviation %f(%f%%).\n", funcorfilename.c_str(),DeEnBaseLoopTime, median,mad, mad/median);
            }
        } else { //Average
            average = 0;
            for(unsigned long long i = truncateN; i < BenchmarkLoopTime - truncateN; ++i) {
                average = average + microsecond.at(i);
            }
            average = average / (double)(BenchmarkLoopTime - truncateN * 2);
            if(BenchmarkFormatCsv) {
                printf("%f,",average);
            } else {
                printf("%s run %llu time Average %f microsecond.\n", funcorfilename.c_str(),DeEnBaseLoopTime,average);
            }
        }
        hlst.clear();
    }
    if(BenchmarkFormatCsv) {
        printf("\n");
    }
}

int main() {
    #if ERICHPACKDEBUGFLAG == 1
    if(std::setvbuf(stdout, nullptr, _IONBF, 0) != 0){
        printf("Fail at set stdout buffer to _IONBF.\n");
    }
    #endif // ERICHPACKDEBUGFLAG
    //TestItem();
    //RandomItem();
    //BenchmarkItem();
    //DynamicTableUpdateItem();

    StatisticLoadItem(false);
    return 0;
}
