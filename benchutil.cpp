#include "core/hpack.h"

#include <array>
#include <fstream>
#include <functional>

using EricHpack::KeyValPair;

typedef std::vector<std::pair<std::string,std::string> > HeaderVec;

struct HeaderVecPair {
    HeaderVec req, resp;
    HeaderVecPair():req(), resp() {
        ;
    }
    HeaderVecPair(const HeaderVec &q,const HeaderVec &p):req(q), resp(p) {
        ;
    }
};

struct FuncNameFunc {
  public:
    const char *funcName;
    std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int )> func;
    FuncNameFunc(const char *funcname, std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int )> f):funcName(funcname), func(f) {
        ;
    }

};

struct SubFlag {
  public:
    static const size_t HFDIGenMax = 7;
    static const unsigned char DecodeTyPtr = 1, DecodeTyHFDIPtr = 2,DecodeTyRef = 3, DecodeTyHFDIRef = 4;
    static const unsigned char EncodeTyPtr = 1, EncodeTyExcludePtr = 2, EncodeTyHFDIPtr = 3;
    static const unsigned char EncodeTyRef = 4, EncodeTyExcludeRef = 5, EncodeTyHFDIRef = 6;
    static const unsigned char EncodeTyWay = 7, EncodeTyExcludeWay = 8, EncodeTyHFDIWay = 9;
    static const unsigned char InputVec = 1, InputMem = 2;
    static const unsigned char HFDIAlwaysIndex = 1,HFDIIncrementalIndexing = 2, HFDINeverIndexed = 3, HFDIWithoutIndexing = 4, HFDIPlan0 = 5, HFDIPlan1 = 6, HFDIPlan2 = 7;

    size_t statusLstPosition = 0;
    unsigned char  encodeTy, decodeTy, encodeInput, docodeInput, hFDIGen;
    std::vector<std::string> exclude;
    EricHpack::StringLiteralEncodeWay encodeWay;
    std::vector<std::array<unsigned char, 5> > statusLst;

    SubFlag():encodeTy(1), decodeTy(1), encodeInput(1), docodeInput(1), hFDIGen(1), exclude(), statusLst() {
        ;
    }
    void Print() const {
        const char *decty[4] = {"Ptr", "HFDIPtr", "Ref", "HFDIRef"};
        const char *encty[9] = {"Ptr", "ExcludePtr", "HFDIPtr", "Ref", "ExcludeRef", "HFDIRef", "Way", "ExcludeWay", "HFDIWay"};
        const char *inputstr[2] = {"Vec", "Mem"};
        const char *hfdistr[HFDIGenMax] = {"AlwaysIndex","IncrementalIndexing","NeverIndexed","WithoutIndexing", "HFDIPlan0", "HFDIPlan1","HFDIPlan2"};
        printf("Input%s EncodeTy%s Input%s DecodeTy%s\n",inputstr[encodeInput-1],encty[encodeTy-1],inputstr[docodeInput-1], decty[decodeTy-1]);
        if(IsEncodeHFDI()) {
            printf("Encode HFDI GEN %s\n", hfdistr[hFDIGen-1]);
        }
        if(IsEncodeExclude()) {
            printf("Exclude:");
            for(const std::string &i : exclude) {
                printf("%s,", i.c_str());
            }
            printf("\n");
        }
    }
    void StatusListGen() {
        // Order encodeTy, decodeTy, encodeInput, docodeInput, hFDIGen
        bool stop = false;
        std::array<unsigned char, 5> lstmax = {10,5,2,3,2}, lst, _lst;
        const std::array<unsigned char, 5> initlst = {1,1,1,1,1};

        statusLst.clear();
        lst = initlst;
        while(!stop) {
            statusLst.push_back(lst);
            if(_IsEncodeHFDI(lst[0])) {
                //hFDIGen only use in EncodeHFDI
                _lst = lst;
                for(size_t i = 2 ; i < HFDIGenMax + 1; ++i) {
                    _lst[4] = i;
                    statusLst.push_back(_lst);
                }
            }
            lst[0] += 1;
            if(lst.at(0) == lstmax.at(0)) {
                lst[0] = initlst.at(0);
                for(size_t i = 1; i < lstmax.size(); ++i) {
                    if((++lst[i]) < lstmax.at(i)) {
                        break;
                    } else {
                        if(i == lstmax.size() - 1) {
                            stop = true;
                            break;
                        }
                        lst[i] = initlst.at(i);
                    }
                }
            }
        }
        return;
    }
    void StatusSet(const size_t pos) {
        // Order encodeTy, decodeTy, encodeInput, docodeInput, hFDIGen
        encodeTy = statusLst.at(pos).at(0);
        decodeTy = statusLst.at(pos).at(1);
        encodeInput= statusLst.at(pos).at(2);
        docodeInput  = statusLst.at(pos).at(3);
        hFDIGen = statusLst.at(pos).at(4);
        return;
    }
    bool StatusNext() {
        if(++statusLstPosition < statusLst.size()) {
            StatusSet(statusLstPosition);
            return true;
        }
        statusLstPosition = 0;
        return false;
    }

    static inline bool _IsEncodeHFDI(unsigned char _encodeTy) {
        return _encodeTy % 3 == 0;
    }

    bool IsEncodeWay()const {
        return encodeTy >= 7 && encodeTy <= 9;
    }
    bool IsEncodeExclude()const {
        return encodeTy % 3 == 2;
    }

    bool IsEncodeHFDI()const {
        return _IsEncodeHFDI(encodeTy);
    }
    bool IsDecodeHFDI()const {
        return decodeTy % 2 == 0;
    }
};

struct InputMemory {
    const size_t sizeByte;
    unsigned char *start;
    InputMemory(const std::vector<unsigned char> &ss):sizeByte(ss.size()), start(nullptr) {
        start = new unsigned char[sizeByte];
        if(start == nullptr) {
            printf("InputMemory::Constructor can not new memory.\n");
        }
        for(size_t i = 0; i < sizeByte; ++i) {
            *(start + i) = ss.at(i);
        }
    }
    ~InputMemory() {
        delete [] start;
    }
};

struct Boob {
    static const size_t buffMax = 3072;
    FILE *fp;
    const char *fileName;
    unsigned char buff[buffMax];
    Boob(const char *filename):fp(nullptr), fileName(filename), buff() {
        fp = fopen(filename, "rb");
        if(fp == nullptr) {
            printf("Boob::Constructor can not open %s.\n", filename);
            fflush(stdout);
        }
    }
    ~Boob() {
        fclose(fp);
    }
    bool ReadnByte(size_t n) {
        if( fread(buff, n, 1, fp) != 1) {
            if(ferror(fp) != 0) {
                printf("Boob::ReadnByte %s Ferror set.\n", fileName);
            }
            return false;
        }
        return true;
    }
    bool KeyAsciiValRandom(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
        size_t cn = 0,wb;
        KeyValPair kvp;
        reqheader.clear();
        resphrader.clear();
        while(cn < buffMax) {
            wb = (cn % 25) + 4;
            if(ReadnByte(wb)) {
                cn += wb;
                kvp.first.append((cn % 24) + 1, 'a' + (cn % 26));
                cn = cn + (cn % 24) + 1;
                for(size_t i = 0; i< wb; ++i) {
                    kvp.second.push_back(buff[i]);
                }
                reqheader.push_back(kvp);
                resphrader.push_back(kvp);
            } else {
                return false;
            }
            kvp.first.clear();
            kvp.second.clear();
        }
        return true;
    }
    bool KeyRandomValRandom(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
        size_t cn = 0,wb;
        std::string key, val;
        reqheader.clear();
        resphrader.clear();
        while(cn < buffMax) {
            wb = (cn % 120) + 4;
            if(ReadnByte(wb * 2)) {
                cn += wb * 2;
                for(size_t i = 0; i< wb; ++i) {
                    key.push_back(buff[i]);
                }
                for(size_t i = wb; i < wb*2; ++i) {
                    val.push_back(buff[i]);
                }
                reqheader.push_back(std::make_pair<std::string,std::string>(key.c_str(), val.c_str()));
                resphrader.push_back(std::make_pair<std::string,std::string>(key.c_str(), val.c_str()));
            } else {
                return false;
            }
            key.clear();
            val.clear();
        }
        return true;
    }
    bool HpRBufferRandom(std::vector<unsigned char> &vecbuff, size_t sizebyte = buffMax - 1) {
        if(ReadnByte(sizebyte - 1)) {
            for(size_t i = 0; i < sizebyte; ++i) {
                vecbuff.push_back(buff[i]);
            }
        } else {
            return false;
        }
        return true;
    }
};

struct WrapLoadFucntion {
    static const unsigned char UseFunc = 0;
    static const unsigned char UseHlst = 1;
    static const unsigned char UseBoobOct = 2;
    static const unsigned char UseBoobAscii = 3;

    const unsigned char use;
    std::unique_ptr<Boob> boob;
    std::shared_ptr<std::vector<std::shared_ptr<HeaderVecPair> > > hlst;
    std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int)> func;
    WrapLoadFucntion(const char *filename, bool oct):use(oct ? UseBoobOct : UseBoobAscii), boob(new Boob(filename)),hlst(nullptr),func() {
        ;
    }
    WrapLoadFucntion(std::shared_ptr<std::vector<std::shared_ptr<HeaderVecPair> > > _hlst):use(UseHlst), boob(nullptr),hlst(std::move(_hlst)),func() {
        ;
    }
    WrapLoadFucntion(std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int)> loadfunc):use(UseFunc), boob(nullptr),hlst(nullptr),func(loadfunc) {
        ;
    }
    ~WrapLoadFucntion() = default;

    bool Load(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
        switch(use) {
        case UseFunc:
            return func(reqheader, resphrader, stage);
        case UseHlst:
            if((size_t)stage < hlst->size()) {
                reqheader.assign(hlst->at(stage)->req.begin(),hlst->at(stage)->req.end());
                resphrader.assign(hlst->at(stage)->resp.begin(), hlst->at(stage)->resp.end());
                return true;
            } else {
                return false;
            }
        case UseBoobOct:
            return boob->KeyRandomValRandom(reqheader, resphrader, stage);
        case UseBoobAscii:
            return boob->KeyAsciiValRandom(reqheader, resphrader, stage);
        default:
            printf("WrapLoadFucntion::Load use not allow.\n");
            return false;
        }
    }
    static std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int)> Bind(WrapLoadFucntion *self) {
        if(self == nullptr) {
            printf("WrapLoadFucntion::Bind receive nullptr self.\n");
        }
        std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int)> f = std::bind(&WrapLoadFucntion::Load, self, std::placeholders::_1, std::placeholders::_2,std::placeholders::_3);
        return f;
    }
};
std::string String2Hex(const std::string &s) {
    std::string r;
    const unsigned char trtable[17] = "0123456789abcdef";
    for(const unsigned char c : s) {
        r.append(1, trtable[(c & 0xf0) >> 4]);
        r.append(1, trtable[c & 0x0f]);
    }
    return r;
}

bool OctetAll(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
    bool proceed = true;
    KeyValPair kvp;
    std::string tmpstr;

    switch(stage) {
    case 0:
        for(size_t i = 0; i<256; ++i) {
            tmpstr.append(1, (unsigned char)i);
            if((i+1) % 16 == 0) {
                kvp.first.assign(tmpstr.begin(), tmpstr.begin() + (tmpstr.size()/2));
                kvp.second.assign(tmpstr.begin() + (tmpstr.size()/2),tmpstr.end());
                reqheader.push_back(kvp);
                resphrader.push_back(kvp);
                tmpstr.clear();
                kvp.first.clear();
                kvp.second.clear();
            }
        }
        break;
    case 1:
        for(size_t i = 0; i<256; ++i) {
            tmpstr.assign(8, (unsigned char)i);
            kvp.first.assign(tmpstr.begin(),tmpstr.end());
            kvp.second.assign(tmpstr.begin(),tmpstr.end());
            reqheader.push_back(kvp);
            resphrader.push_back(kvp);
        }
        break;
    default:
        proceed = false;
        break;
    }
    return proceed;
}

bool LargeKvp(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
    bool proceed = true;
    KeyValPair kvp;

    switch(stage) {
    case 0:
    case 1:
    case 2:
        //case 3:
        //case 4:
        for(size_t i = 0; i< 1024 * ((size_t)stage + 1); ++i) {
            kvp.first.append(1, (unsigned char)((i % 94) + 32));
            kvp.second.append(1, (unsigned char)((i % 94) + 32));
        }
        for(size_t i = 0; i<2; ++i) {
            reqheader.push_back(kvp);
            resphrader.push_back(kvp);
        }
        break;
    default:
        proceed = false;
        break;
    }
    return proceed;
}

bool EmptyKeyValPair(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
    bool proceed = true;

    switch(stage) {
    case 0:
        reqheader.push_back(std::make_pair<std::string, std::string>("accept-charset",""));
        reqheader.push_back(std::make_pair<std::string, std::string>("accept-encoding",""));
        reqheader.push_back(std::make_pair<std::string, std::string>(":method",""));
        reqheader.push_back(std::make_pair<std::string, std::string>(":status",""));
        resphrader.push_back(std::make_pair<std::string, std::string>("accept-charset",""));
        resphrader.push_back(std::make_pair<std::string, std::string>("accept-encoding",""));
        resphrader.push_back(std::make_pair<std::string, std::string>(":method",""));
        resphrader.push_back(std::make_pair<std::string, std::string>(":status",""));
        break;
    case 1:
        //return true;
        reqheader.push_back(std::make_pair<std::string, std::string>("",""));
        resphrader.push_back(std::make_pair<std::string, std::string>("",""));
        break;
    case 2:
        for(size_t i =0; i< 12; ++i) {
            reqheader.push_back(std::make_pair<std::string, std::string>("",""));
            resphrader.push_back(std::make_pair<std::string, std::string>("",""));
        }
        break;
    case 3:
        reqheader.push_back(std::make_pair<std::string, std::string>("","http"));
        reqheader.push_back(std::make_pair<std::string, std::string>("","https"));
        reqheader.push_back(std::make_pair<std::string, std::string>("","206"));
        reqheader.push_back(std::make_pair<std::string, std::string>("","XYZ"));
        resphrader.push_back(std::make_pair<std::string, std::string>("","http"));
        resphrader.push_back(std::make_pair<std::string, std::string>("","https"));
        resphrader.push_back(std::make_pair<std::string, std::string>("","206"));
        resphrader.push_back(std::make_pair<std::string, std::string>("","XYZ"));
        break;
    /*
    case 4:
        break;
    case 5:
        break;
    */
    default:
        proceed = false;
        break;
    }
    return proceed;
}

const std::array<const char*, 4> LoadBoobLst = {"bench/Lucky_Kids.mp3","bench/Superman_Kid.mp3","bench/p-Groups.mp4", "bench/previous_examination.mp4"};
const std::array<FuncNameFunc, 3> LoadFuncLst = {FuncNameFunc("EmptyKeyValPair",EmptyKeyValPair),FuncNameFunc("OctetAll",OctetAll),FuncNameFunc("LargeKvp",LargeKvp)};
const std::array<const char*, 12> BenchDataFileName = {"bench/template_parameters.data","bench/Gophercon.data","bench/Hlp.data","bench/Cppvec.data","bench/content_type.data","bench/example_domains.data","bench/clang_command.data","bench/cc_sa.data","bench/go_http.data","bench/HttplightSitemap.data","bench/NginxSitemap.data","bench/HaproxySitemap.data"};

void PrintVecHeaderVecPair(std::vector<std::shared_ptr<HeaderVecPair> > hlst, bool ashex = false) {
    for(auto i : hlst) {
        printf("Request\n");
        for(auto j : i->req) {
            if(ashex) {
                printf("%s : %s\n", String2Hex(j.first).c_str(), String2Hex(j.second).c_str());
            } else {
                printf("%s : %s\n", j.first.c_str(), j.second.c_str());
            }
        }
        printf("Respones\n");
        for(auto j : i->resp) {
            if(ashex) {
                printf("%s : %s\n", String2Hex(j.first).c_str(), String2Hex(j.second).c_str());
            } else {
                printf("%s : %s\n", j.first.c_str(), j.second.c_str());
            }
        }
    }
    return;
}

void PrintHfdiList(const std::vector<EricHpack::HeaderFieldInfo> &hfdilst) {
    for(const EricHpack::HeaderFieldInfo &hfdi : hfdilst) {
        printf("Repr %s evictCounter %lu.\n", EricHpack::Hpack::HeaderFieldRepr2String(hfdi.representation).c_str(), hfdi.evictCounter);
    }
}

bool ParserKeyVal(std::string &line,const size_t linecn,HeaderVec &vec) {
    bool escape, exist;
    size_t poscn;
    std::string keystr,valuestr,*wstr;
    unsigned char stat;

    stat = 0;
    poscn = 0;
    exist = false;
    line += " ";
    wstr = &keystr;
    escape = false;
    for(char c : line) {
        switch(c) {
        case ' ':
            if(stat == 1 || stat == 3) {
                if(escape) {
                    escape = false;
                    wstr->append(1, '\\');
                }
                wstr->append(1, ' ');
            } else {
                ;
            }
            break;
        case '"':
            if(stat == 0 || stat == 2) {
                stat += 1;
                if(stat == 3) {
                    wstr = &valuestr;
                }
            } else if(stat == 1 || stat == 3) {
                if(escape) {
                    escape = false;
                    wstr->append(1, '"');
                } else {
                    stat += 1;
                    if(stat == 4) {
                        exist = true;
                    }
                }
            }
            break;
        case '\\':
            if(stat == 1 || stat == 3) {
                escape = true;
            } else {
                exist = true;
                printf("Line %lu Pos %lu Unexpect \\.\n", linecn, poscn);
            }
            break;
        default:
            if(stat == 1 || stat == 3) {
                if(escape) {
                    escape = false;
                    wstr->append(1, '\\');
                }
                wstr->append(1, c);
            } else {
                exist = true;
                printf("Line %lu Pos %lu Unexpect '%c'.\n", linecn, poscn, c);
            }
            break;
        }
        if(exist) {
            break;
        }
        ++poscn;
    }
    if(stat == 4) {
        vec.push_back(std::make_pair<std::string,std::string>(keystr.c_str(), valuestr.c_str()));
        return true;
    } else {
        printf("Line %lu stop at Unexpect status(%u).\n", linecn, stat);
        return false;
    }
}

bool LoadBenchmarkData(std::string filename, std::vector<std::shared_ptr<HeaderVecPair> > &hlst) {
    bool exit, readfn;
    size_t linecn;
    char funcname[256];
    HeaderVec *vec;
    std::string line;
    std::ifstream fd;
    std::shared_ptr<HeaderVecPair> hvp;

    fd.open(filename, std::ios::in);
    if(!fd.is_open()) {
        printf("Can not open %s.\n", filename.c_str());
    }

    vec = nullptr;
    exit = false;
    linecn = 0;
    readfn = false;
    while(!fd.eof()) {
        ++linecn;
        std::getline(fd, line);
        if(line.size() == 0) {
            continue;
        }
        switch(line.at(0)) {
        case '#':
            break;
        case 'F':
            if(! (sscanf(line.c_str(), "FunctionName %255s", funcname) == 1)) {
                exit = true;
                printf("Line %lu not follow \"FunctionName %%255s\" format(%s).\n", linecn, line.c_str());
            }
            if(readfn) {
                exit = true;
                printf("Line %lu only allow one function in file.\n", linecn);
            }
            vec = nullptr;
            readfn = true;
            break;
        case 'Q':
            if(hvp) {
                hlst.push_back(hvp);
            }
            hvp = std::make_shared<HeaderVecPair>();
            vec = &(hvp->req);
            break;
        case 'P':
            if(!hvp) {
                exit = true;
                printf("Line %lu Use \"FunctionName %%255s\" before P.\n", linecn);
            }
            vec = &(hvp->resp);
            break;
        case '"':
            if(vec == nullptr) {
                exit = true;
                printf("Line %lu Use Q or P before \"%%8191s\"\"%%8191s\".\n", linecn);
            }
            if(!ParserKeyVal(line, linecn, *vec)) {
                printf("Line %lu not follow \"%%8191s\"\"%%8191s\" format(%s).\n", linecn, line.c_str());
            }
            break;
        default:
            printf("Line %lu Unknow(%s).\n", linecn, line.c_str());
        }
        if(exit) {
            break;
        }
    }
    bool finishSuccess = true;
    if(!fd.eof()) {
        finishSuccess = false;
    }
    fd.close();
    return finishSuccess;
}

void PerpareHeader(std::vector<std::shared_ptr<HeaderVecPair> > &hlst, std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int )> funcptr) {
    int stage;
    HeaderVec qheader, pheader;
    std::shared_ptr<HeaderVecPair> hvp;

    stage = 0;
    hvp = std::make_shared<HeaderVecPair>();
    while(funcptr(hvp->req,hvp->resp, stage++)) {
        hlst.push_back(hvp);
        hvp = std::make_shared<HeaderVecPair>();
    }
    return;
}

bool _LoadItem(const int testDataSet, const bool rthlst,std::string &funcname, int stage, std::shared_ptr<std::vector<std::shared_ptr<HeaderVecPair> > > hlst, std::unique_ptr<WrapLoadFucntion> &wrapfunc) {
    if(testDataSet == 0) {
        return false;
    }
    if((size_t)stage < LoadFuncLst.size()) {
        funcname.assign(LoadFuncLst.at(stage).funcName);
        if(rthlst) {
            PerpareHeader(*(hlst.get()), LoadFuncLst.at(stage).func);
        } else {
            wrapfunc.reset(new WrapLoadFucntion(LoadFuncLst.at(stage).func));
        }
        return true;
    }
    if(testDataSet <= 1) {
        return false;
    }
    stage = stage - LoadFuncLst.size();
    if((size_t)stage < BenchDataFileName.size()) {
        funcname.assign(BenchDataFileName.at(stage));
        LoadBenchmarkData(BenchDataFileName.at(stage),*(hlst.get()));
        if(!rthlst) {
            wrapfunc.reset(new WrapLoadFucntion(hlst));
        }
        return true;
    }
    if(testDataSet <= 2) {
        return false;
    }
    stage = stage - BenchDataFileName.size();
    if((size_t)stage < LoadBoobLst.size()) {
        funcname.assign(LoadBoobLst.at(stage));
        if(rthlst) {
            Boob boob(LoadBoobLst.at(stage));
            PerpareHeader(*(hlst.get()), std::bind(&Boob::KeyRandomValRandom, &boob, std::placeholders::_1, std::placeholders::_2,std::placeholders::_3));
        } else {
            wrapfunc.reset(new WrapLoadFucntion(LoadBoobLst.at(stage), true));
        }
        return true;
    }
    if(testDataSet <= 3) {
        return false;
    }
    stage = stage - BenchDataFileName.size();
    if((size_t)stage < LoadBoobLst.size()) {
        funcname.assign(LoadBoobLst.at(stage));
        if(rthlst) {
            Boob boob(LoadBoobLst.at(stage));
            PerpareHeader(*(hlst.get()), std::bind(&Boob::KeyAsciiValRandom, &boob, std::placeholders::_1, std::placeholders::_2,std::placeholders::_3));
        } else {
            wrapfunc.reset(new WrapLoadFucntion(LoadBoobLst.at(stage), false));
        }
        return true;
    }
    return false;
}

bool LoadTestItem(std::string &funcname, int stage,std::unique_ptr<WrapLoadFucntion> &wrapfunc) {
    wrapfunc.reset(nullptr);
    return _LoadItem(4, false, funcname,stage, std::make_shared<std::vector<std::shared_ptr<HeaderVecPair> > >(), wrapfunc);
}

struct NullDeleterHack {
    template<class T>
    void operator()(T* p) {
        ;
    }
};

bool LoadBenchmarkItem(std::string &funcname, std::vector<std::shared_ptr<HeaderVecPair> > &hlst, int &stage) {
    std::unique_ptr<WrapLoadFucntion> wrapfunc;
    std::shared_ptr<std::vector<std::shared_ptr<HeaderVecPair> > > hptr(&hlst, NullDeleterHack {});
    return _LoadItem(2, true, funcname,stage, hptr, wrapfunc);
}

bool LoadDynamicTableUpdateItem(std::string &funcname, std::vector<std::shared_ptr<HeaderVecPair> > &hlst, int &stage) {
    std::unique_ptr<WrapLoadFucntion> wrapfunc;
    std::shared_ptr<std::vector<std::shared_ptr<HeaderVecPair> > > hptr(&hlst, NullDeleterHack {});
    return _LoadItem(4, true, funcname,stage, hptr, wrapfunc);
}

void StatisticLoadItem(bool detail = false) {
    int stage, loadstage;
    std::string filename;
    const int testDataSetMax = 4;
    HeaderVecPair wphvp;
    std::unique_ptr<WrapLoadFucntion> wrapfunc;
    std::vector<std::shared_ptr<HeaderVecPair> >::iterator hvpit;
    std::shared_ptr<std::vector<std::shared_ptr<HeaderVecPair> > > hptr;
    unsigned long sstreqheader, sstrespheader, sstreqheaderchar, sstrespheaderchar;

    auto _loadItem_parameter = [&](void) -> std::string{
        char buff[256];
        std::string str;
        snprintf(buff, 255, "StatisticLoadItem::_LoadItem(%jd, 0:1, %s, %jd, ...)",(intmax_t)testDataSetMax, filename.c_str(), (intmax_t)loadstage-1);
        str.assign(buff);
        return str;
    };
    auto _word_statistic = [&](HeaderVec &fhdp,HeaderVec &bhdp,unsigned long &headercn,unsigned long &wordcn) mutable ->void{
        unsigned long fhdc, bhdc, fwdc, bwdc;
        fhdc = bhdc = fwdc = bwdc = 0;
        fhdc += fhdp.size();
        bhdc += bhdp.size();
        for(KeyValPair &kvp : fhdp) {
            fwdc += kvp.first.size();
            fwdc += kvp.second.size();
        }
        for(KeyValPair &kvp : bhdp) {
            bwdc += kvp.first.size();
            bwdc += kvp.second.size();
        }
        #if RESULTCHECK
        if(fhdc != bhdc) {
            printf("%s Fail HeaderLine(%ju:ju) not same.\n", _loadItem_parameter().c_str(),(uintmax_t)fhdc, (uintmax_t)bhdc );
        }
        if(fwdc != bwdc) {
            printf("%s Fail HeaderWordCount(%ju:ju) not same.\n", _loadItem_parameter().c_str(),(uintmax_t)fwdc, (uintmax_t)bhdc );
        }
        #endif // RESULTCHECK

        headercn += fhdc;
        wordcn += fwdc;
    };
    auto _print_statistic = [&](void) -> void{
        if(!detail) {
            printf("%s have %d stage reqheader(line:%ju,word:%ju) resphrader(line:%ju,word:%ju)\n", filename.c_str(), stage, (uintmax_t)sstreqheader, (uintmax_t)sstreqheaderchar, (uintmax_t)sstrespheader, (uintmax_t)sstrespheaderchar);
        }
    };
    auto _op_sub = [&] (std::shared_ptr<HeaderVecPair> hphvp, HeaderVecPair &iwphvp) mutable -> void{
        _word_statistic(hphvp->req,iwphvp.req, sstreqheader, sstreqheaderchar);
        _word_statistic(hphvp->resp,iwphvp.resp, sstrespheader, sstrespheaderchar);
        if(detail) {
            printf("%s %d reqheader(%zu) resphrader(%zu)\n", filename.c_str(), stage, hphvp->req.size(), hphvp->resp.size());
        }
    };
    auto _hvp_next = [&](void) mutable -> bool{
        bool hpnext, wpnext;
        hpnext = (hvpit != hptr->end());
        wpnext = WrapLoadFucntion::Bind(wrapfunc.get())(wphvp.req, wphvp.resp, stage);
        if(hpnext != wpnext) {
#if RESULTCHECK
            printf("%s Fail at stage(%jd) _hvp_next retrun status.\n",_loadItem_parameter().c_str(), (intmax_t)stage);
#endif // RESULTCHECK
            return false;
        }
        return hpnext;
    };
    auto _loadItem = [&](void) mutable -> bool{
        bool ldithptr, lditwrap;
        std::string hptrfilename, wrapfilename;
        std::unique_ptr<WrapLoadFucntion> notusewrapfunc;

        ldithptr = _LoadItem(testDataSetMax, true, hptrfilename, loadstage, hptr, notusewrapfunc);
        lditwrap = _LoadItem(testDataSetMax, false, wrapfilename, loadstage++, std::make_shared<std::vector<std::shared_ptr<HeaderVecPair> > >(), wrapfunc);
        if(ldithptr != lditwrap) {
#if RESULTCHECK
            printf("%s Fail return value not same.\n",_loadItem_parameter().c_str());
#endif // RESULTCHECK
            return false;
        }
        filename = hptrfilename;
#if RESULTCHECK
        if(hptrfilename != wrapfilename) {
            printf("StatisticLoadItem::_LoadItem(%jd, 0:1, filename, %jd, ...) Fail set not same filename(%s:%s).\n",(intmax_t)testDataSetMax, (intmax_t)loadstage-1, hptrfilename.c_str(), wrapfilename.c_str());
        }
#endif // RESULTCHECK
        return ldithptr;
    };
    loadstage = 0;
    hptr = std::make_shared<std::vector<std::shared_ptr<HeaderVecPair> > >();
    while(_loadItem()) {
        stage = 0;
        hvpit = hptr->begin();
        sstreqheader = sstrespheader = sstreqheaderchar = sstrespheaderchar =0;
        while(_hvp_next()) {
            _op_sub((*hvpit), wphvp);
            ++stage;
            hvpit++;
            wphvp.req.clear();
            wphvp.resp.clear();
        }
        _print_statistic();
        hptr = std::make_shared<std::vector<std::shared_ptr<HeaderVecPair> > >();
    }
}

void DeEnFailCheck(const HeaderVec &f, const HeaderVec &b,const std::string &failat,EricHpack::HpackStatus enstat,EricHpack::HpackStatus destat) {
    if(enstat != EricHpack::HpackStatus::Success) {
        printf("Hpack Encode fail %s at %s.\n", EricHpack::Hpack::HpackStatus2String(enstat).c_str(), failat.c_str());
        return;
    }
    if(destat != EricHpack::HpackStatus::Success) {
        printf("Hpack Decode fail %s at %s.\n", EricHpack::Hpack::HpackStatus2String(destat).c_str(), failat.c_str());
        return;
    }
    if(f.size() != b.size()) {
        printf("Hpack DeEn header not same length En %lu De %lu  at %s.\n", f.size(), b.size(), failat.c_str());
        return;
    }
    if(f != b) {
        printf("Hpack DeEn %s Fail.\n", failat.c_str());
        for(size_t i = 0; i < f.size(); ++i) {
            if(f.at(i) != b.at(i)) {
                //KeyValPairPrint(f.at(i));
                //KeyValPairPrint(b.at(i));
                printf("%lu Fail\n", i);
                printf("%s : %s\n", String2Hex(f.at(i).first).c_str(), String2Hex(f.at(i).second).c_str());
                printf("%s : %s\n", String2Hex(b.at(i).first).c_str(), String2Hex(b.at(i).second).c_str());
            } else {
                printf("%lu Pass\n", i);
                printf("%s : %s\n", String2Hex(f.at(i).first).c_str(), String2Hex(f.at(i).second).c_str());
            }
        }
    }
    return;
}

void SubCheckClear(const HeaderVec &f,HeaderVec &headerclear,const std::string &failat,EricHpack::HpackStatus enstat,EricHpack::HpackStatus destat,const EricHpack::Hpack &fhpa,const EricHpack::Hpack &bhpa, const SubFlag &subflag, std::vector<EricHpack::HeaderFieldInfo> &ehfdi,std::vector<EricHpack::HeaderFieldInfo> &dhfdi) {
#if RESULTCHECK == 1
    DeEnFailCheck(f, headerclear, failat, enstat, destat);
    if(fhpa.DynamicTableMaxSize() != bhpa.DynamicTableMaxSize()) {
        printf("Hpack DynamicTableMaxSize not same En %lu De %lu at %s.\n", fhpa.DynamicTableMaxSize(), bhpa.DynamicTableMaxSize(), failat.c_str());
    }
    if(fhpa.DynamicTableSize() != bhpa.DynamicTableSize()) {
        printf("Hpack DynamicTableSize not same En %lu De %lu at %s.\n", fhpa.DynamicTableSize(), bhpa.DynamicTableSize(), failat.c_str());
    }
    if(subflag.IsEncodeHFDI()&& subflag.IsDecodeHFDI()) {
        if(ehfdi.size() != dhfdi.size()) {
            printf("Hpack HFDIlst not same length En %lu De %lu at %s.\n", ehfdi.size(), dhfdi.size(), failat.c_str());
        }
        if(ehfdi != dhfdi) {
            printf("Hpack HFDIlst %s fail.\n", failat.c_str());
            for(size_t i = 0; i < ehfdi.size(); ++i) {
                if(ehfdi.at(i) == dhfdi.at(i)) {
                    printf("%lu Pass\n", i);
                    printf("evictCounter %lu representation %s\n", ehfdi.at(i).evictCounter, EricHpack::Hpack::HeaderFieldRepr2String(ehfdi.at(i).representation).c_str());
                } else {
                    printf("%lu Fail\n", i);
                    printf("evictCounter %lu representation %s\n", ehfdi.at(i).evictCounter, EricHpack::Hpack::HeaderFieldRepr2String(ehfdi.at(i).representation).c_str());
                    printf("evictCounter %lu representation %s\n", dhfdi.at(i).evictCounter, EricHpack::Hpack::HeaderFieldRepr2String(dhfdi.at(i).representation).c_str());
                }
            }
        }

    }
#endif // RESULTCHECK
    ehfdi.clear();
    dhfdi.clear();
    headerclear.clear();
    return;
}
